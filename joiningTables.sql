--Table Joins
select * from publishers;
select * from pub_info ;
select * from titles ;
select * from sales;
select * from stores ;
select * from discounts d ;
select * from authors a ;
--1. Join the publishers and pub_info and show the publisher name and the first 40 characters of the pr_info information.
select p.pub_name as publisher, substring(pi2.pr_info, 1,40) as pr_info from publishers p 
join pub_info pi2
on p.pub_id = pi2.pub_id ;

--2. Join the publishers and titles tables to show all titles published by each publisher. Display the pub_id ,  pub_name and and  title_id
select p.pub_id, p.pub_name, t.title_id from publishers p 
join titles t 
on p.pub_id = t.pub_id ;

--3. For each title_id in the table titles, rollup the corresponding qty in sales and show: title_id, 
--title, ord_num and the rolled-up value as a column aggregate called Total Sold
select t.title_id, t.title, s.ord_num, s.qty as TotalSold from titles t 
join sales s 
on t.title_id  = s.title_id 
group by t.title_id, t.title, s.ord_num, rollup (TotalSold);

-- 4. For each stor_id in stores, show the corresponding ord_num in sales and the discount type from table discounts . 
--The output should consist of three columns: ord_num , discount and discounttype and should be sorted on ord_num
select s.ord_num as ord_num, d.discount as discount, d.discounttype as discounttype from sales s 
join discounts d 
on s.stor_id  = d.stor_id 
order by ord_num ;

--5. Show  au_lname  from  authors , and  pub_name  from  publishers  when both publisher and author live in the same city.
select a.au_lname, p.pub_name from authors a 
join publishers p 
on a.city  = p.city;

-- 6. Refine 5 so that for each author you show all publishers who live in the same city and have published one of the authors titles.

select a.au_lname, p.pub_name, t.title from authors a 
join publishers p
on a.city  = p.city
join titles t
on p.pub_id  = t.pub_id 

--7. Refine 1 so that an outer join is performed. All of the publishers from the first table should be shown, not just those with pr_info information in pub_info. You should use the ANSI SQL92 syntax.
select p.pub_name as publisher, substring(pi2.pr_info, 1,40) as pr_info from publishers p 
left outer join pub_info pi2
on p.pub_id = pi2.pub_id ;

--8. List each publisher's name, the title of each book they have sold and the total quantity of that title.

select p.pub_name as publisher_name, t.title as title, count(t.title) as total_quantity
from publishers p 
join titles t 
on t.pub_id = p.pub_id 
group by publisher_name, title
