--Grouping and Aggregate Functions
--Get average prices from the titles table for each type of book, and convert type to char(30).
select cast(avg(price::numeric)as char(30)) from titles group by price;

--Print the difference between (to a resolution of days) the earliest and latest publication date in titles
select * from titles;
select   date_trunc('day',age(max(pubdate),min(pubdate))) from titles 

--Print the average, min and max book prices within the titles table organised into groups based on type and publisher id.
select round(avg(price::numeric),2 ), max(price), min(price), type, pub_id from titles  group by type, pub_id ; 

--Refine the previous question to show only those types whose average price is > $20 and output the results sorted on the average price.
select round(avg(price::numeric),2 ) as av, max(price) as mx, min(price) as mn, type as ty, pub_id as pb from titles  group by type, pub_id 
having round(avg(price::numeric),2 ) > 20 order by av desc;

--List the books in order of the length of their title
select title from titles order by length(title) desc;
--Business Queries
--What is the average age in months of each type of title?
--option 1
select type, avg(extract(year from age(now(), pubdate))*12 + extract(month from age(now(), pubdate))) from titles group by type, pubdate; 
--option 2
select type, floor(to_char(now() - pubdate, 'DD')::numeric *0.0328767) from titles group by type, pubdate;
--option 3
select type, floor(avg(current_date-pubdate)/12) from titles group by type, pubdate ;
--option 4

select type as type, extract(year from age(pubdate))*12 + extract(month from age(pubdate)) as average_date from titles group by type, pubdate;


--How many authors live in each city?
select * from authors;
select city, count(city) from authors group by city ;

--What is the longest title?
select title, length(title) from titles order by length(title) desc limit 1;
select title, length(title) from titles order by length(title) desc fetch first 1 rows only;

--How many books have been sold by each store and how many books have been sold in total?
select * from stores;
select * from sales ;
select stor_id, count(stor_id), sum(qty) from sales group by stor_id;
