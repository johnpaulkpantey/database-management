--Current Time
--Display the current date
select current_date;

--Display the current time
select current_time;

--Display the current timestamp
select current_timestamp;

--Converting from Strings
--Convert the string '2018-09-26' to a date
select to_date('2018-09-26','YYYYMMDD');

--Convert the string '2018-09-26' to a timestamp
select to_date('2018-09-26','YYYY-MM-DD HH:MI:SS.US-SSSS');


--Exercises
--
--In a SELECT-statement, add the difference between 2011-01-01 and the current date to the sales date field in the sales table.
--select timestampadd() 
--The output should show both the new value and original sales date, with the original in dd-mm-yy format.
--
--Show how many days remain between now and Thanksgiving and/or Christmas and/or New Years Day.
--
--Show how old you are as a number of days - you don't have to be too honest about your year of birth if you are over 25 
--
--For each of the publication dates in titles, display the difference between the pubdate and the current date in days as an integer.
--
--From the sales table convert the date field to dd/mm/yy format
--
--select from the 
--sales
-- table 
--stor_id
-- for sales on your birthday in "dd/mm/yy" format. If there are none, then pick sales for the date nearest to your birthdate. Search again for before your birthday.
--
--Study the reference documents for your DBMS and then display each book title with its publication date converted to the correct format for each of the following regions:
--
--USA
--UK
--Japan
--Show the first word of each title.
--
--Print the number of characters which could be added to each of the values in the title field before the data is truncated. You can find the title column's length using the system commands for your DBMS.
--
--Replace the first space in the address field in the authors table with the word 
----hello--
--.
--
--Provide a randomly generated price for all titles lacking a price in the titles table. (math functions)
--
--Print all phone numbers from the authors table without the three digit area code.
--
--Capitalise the second character in all last names from the authors table.