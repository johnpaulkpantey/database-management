select * from publishers;
select * from pub_info ;
select * from titles ;
select * from sales;
select * from stores ;
select * from discounts d ;
select * from authors a ;
select * from titleauthor;
--Sub-Queries
--1. Which publishers have published at least one book?
select p.pub_name as name, count(t.title) as title_count
from publishers p 
join titles t 
on p.pub_id = t.pub_id 
group by name

-- Optional *
select p.pub_name from publishers p 
where p.pub_id in (select pub_id from titles)

--Optional
select p.pub_name from publishers p 
where p.pub_id in (select pub_id from titles t group by pub_id having count(t.title)>0)

--2. Which authors have been published by more than one publisher?
-- Tables needed: authors, tileauthor and titles
select a.au_fname, a.au_lname, count(a.au_lname) as numberOfPublisher
from authors a 
join titleauthor t 
on a.au_id = t.au_id 
where  t.title_id  in (select title_id from titles)
group by a.au_lname, a.au_fname
having(count(a.au_lname) >1) order by a.au_fname desc

-- Option 2
select a.au_fname, a.au_lname
from pubs2.authors a 
where a.au_id in (
	select ta.au_id 
	from pubs2.titleauthor ta 
	join pubs2.titles t 
	on t.title_id = ta.title_id
	group by ta.au_id
	having count(t.pub_id) > 1
	)
	order by a.au_fname desc



--3. Which authors live in a city where a publisher exists?
-- needed tables 
	select a.au_fname, a.au_lname, a.city  from 
	authors a where a.city 
	in (select p.city from publishers p)
	
--4. How many authors are there with the same first initial?
--tables needed: authors,
select sum(sub1."Number of initials") from 
(select count(substring(a.au_fname,1,1)) as "Number of initials"
from authors a
group by substring(a.au_fname,1,1)
having count(substring(a.au_fname,1,1))>1) as "sub1";
	


--5. What is the most expensive book?
select * from titles t 
--6. Which is the oldest published book? Which is the youngest?
--
--7. Which books are more expensive than all books of any other type?
--
--8. Which books have an above average price for their type?
--
--9. How much above or below the "average price of all books" is the price for each book?