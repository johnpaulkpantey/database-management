--Find all titles with an undefined price in table titles
select * from titles;
select * from titles where price is null;

--Find all titles with an undefined price in table titles and supply a price of $20.00 for those with no defined price
update titles set price ='$20.00' where price is null;


--List the first 50 characters (of the pr_info column) of the pub_info table
select * from pub_info;
select substring(pr_info,0, 50)  from pub_info;

--Examine the Postgres function reference for alternative ways of converting date values to varchar.

--Locate and use one the functions that would allow you to specify the format to be used when outputting a date. Format the ord_date in sales so it looks like this: Tuesday 13th September 1994
select to_char(ord_date,'Day ddth Month YYYY') from sales;